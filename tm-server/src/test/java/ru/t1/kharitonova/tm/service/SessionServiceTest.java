package ru.t1.kharitonova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.kharitonova.tm.api.service.*;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.dto.model.SessionDTO;
import ru.t1.kharitonova.tm.dto.model.UserDTO;

import java.util.ArrayList;
import java.util.List;

@Ignore
public class SessionServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    protected final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ISessionService sessionService = new SessionService(connectionService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService, projectService, taskService);

    @NotNull
    private final List<SessionDTO> sessionDTOList = new ArrayList<>();

    @NotNull
    private List<UserDTO> userDTOList = new ArrayList<>();

    @Before
    public void init() {
        @NotNull final UserDTO userDTOAdmin = new UserDTO();
        userDTOAdmin.setLogin("admin");
        userDTOAdmin.setPasswordHash("admin");
        userDTOAdmin.setEmail("admin@test.ru");
        userDTOAdmin.setRole(Role.ADMIN);

        @NotNull final UserDTO userDTOTest = new UserDTO();
        userDTOTest.setLogin("test");
        userDTOTest.setPasswordHash("test");
        userDTOTest.setEmail("test@test.ru");
        userDTOTest.setRole(Role.USUAL);

        if (userService.findByLogin("admin") == null) {
            userService.add(userDTOAdmin);
            userDTOList.add(userDTOAdmin);
        } else {
            userDTOList.add(userService.findByLogin("admin"));
        }
        if (userService.findByLogin("test") == null) {
            userService.add(userDTOTest);
            userDTOList.add(userDTOTest);
        } else {
            userDTOList.add(userService.findByLogin("test"));
        }

        @Nullable final String adminUserId = userDTOAdmin.getId();
        @Nullable final String testUserId = userDTOTest.getId();
        @NotNull final SessionDTO sessionDTOAdmin = new SessionDTO(adminUserId, Role.ADMIN);
        @NotNull final SessionDTO sessionDTOTest = new SessionDTO(testUserId, Role.USUAL);

        if (sessionService.findOneByUserId(adminUserId) == null) {
            sessionDTOList.add(sessionService.create(sessionDTOAdmin));
        } else {
            sessionDTOList.add(sessionService.findOneByUserId(adminUserId));
        }
        if (sessionService.findOneByUserId(testUserId) == null) {
            sessionDTOList.add(sessionService.create(sessionDTOTest));
        } else {
            sessionDTOList.add(sessionService.findOneByUserId(testUserId));
        }
    }

    @After
    public void clear() {
        sessionService.removeAll();
        sessionDTOList.clear();
        userDTOList.clear();
    }

    @Test
    public void testCreate() {
        final int expectedSize = sessionDTOList.size();
        Assert.assertEquals(expectedSize, sessionService.getSize());
        @NotNull final SessionDTO sessionDTO = new SessionDTO(userDTOList.get(1).getId(), Role.USUAL);
        sessionService.create(sessionDTO);
        Assert.assertEquals(expectedSize + 1, sessionService.getSize());
    }


    @Test
    public void testExistsByIdPositive() {
        for (@NotNull final SessionDTO sessionDTO : sessionDTOList) {
            @NotNull final String id = sessionDTO.getId();
            Assert.assertTrue(sessionService.existsById(id));
        }
    }

    @Test
    public void testExistsByIdNegative() {
        Assert.assertFalse(sessionService.existsById("Other_Id"));
    }

    @Test
    public void testFindOneByIdPositive() {
        for (@NotNull final SessionDTO sessionDTO : sessionDTOList) {
            @NotNull final String id = sessionDTO.getId();
            @Nullable final SessionDTO sessionDTO1 = sessionService.findOneById(id);
            Assert.assertNotNull(sessionDTO1);
            Assert.assertEquals(id, sessionDTO1.getId());
        }
    }

    @Test
    public void testFindOneByIdNegative() {
        @NotNull final String id = "Other_id";
        @Nullable final SessionDTO sessionDTO = sessionService.findOneById(id);
        Assert.assertNull(sessionDTO);
    }

    @Test
    public void testGetSize() {
        final int projectSize = sessionService.getSize();
        Assert.assertEquals(sessionDTOList.size(), projectSize);
    }

    @Test
    public void testRemoveById() {
        for (int i = 0; i < sessionDTOList.size(); i++) {
            @NotNull final SessionDTO sessionDTO = sessionDTOList.get(i);
            sessionService.removeById(sessionDTO.getId());
            Assert.assertEquals(sessionService.getSize(), sessionDTOList.size() - i - 1);
        }
        Assert.assertEquals(0, sessionService.getSize());
    }

    @Test
    public void testRemoveByIdNegative() {
        @NotNull final String id = "Other_Id";
        sessionService.removeById(id);
        Assert.assertEquals(sessionDTOList.size(), sessionService.getSize());
    }

    @Test
    public void testRemoveAll() {
        sessionService.removeAll();
        Assert.assertEquals(0, sessionService.getSize());
    }

}
