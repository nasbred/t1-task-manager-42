package ru.t1.kharitonova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.kharitonova.tm.api.service.*;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.exception.entity.TaskNotFoundException;
import ru.t1.kharitonova.tm.exception.field.IndexIncorrectException;
import ru.t1.kharitonova.tm.exception.field.NameEmptyException;
import ru.t1.kharitonova.tm.exception.field.UserIdEmptyException;
import ru.t1.kharitonova.tm.dto.model.ProjectDTO;
import ru.t1.kharitonova.tm.dto.model.TaskDTO;
import ru.t1.kharitonova.tm.dto.model.UserDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Ignore
public class TaskServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    protected final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService, projectService, taskService);

    @NotNull
    private List<ProjectDTO> projectDTOList;

    @NotNull
    private List<TaskDTO> taskDTOList;

    @NotNull
    private List<UserDTO> userDTOList;

    @Before
    public void init() {
        projectDTOList = new ArrayList<>();
        taskDTOList = new ArrayList<>();
        userDTOList = new ArrayList<>();

        @NotNull final UserDTO userDTOAdmin = new UserDTO();
        @NotNull final String adminId = userDTOAdmin.getId();
        userDTOAdmin.setLogin("admin");
        userDTOAdmin.setPasswordHash("admin");
        userDTOAdmin.setEmail("admin@test.ru");
        userDTOAdmin.setRole(Role.ADMIN);

        @NotNull final UserDTO userDTOTest = new UserDTO();
        @NotNull final String userId = userDTOTest.getId();
        userDTOTest.setLogin("test");
        userDTOTest.setPasswordHash("test");
        userDTOTest.setEmail("test@test.ru");
        userDTOTest.setRole(Role.USUAL);

        if (userService.findByLogin("admin") == null) {
            userService.add(userDTOAdmin);
            userDTOList.add(userDTOAdmin);
        } else {
            userDTOList.add(userService.findByLogin("admin"));
        }
        if (userService.findByLogin("test") == null) {
            userService.add(userDTOTest);
            userDTOList.add(userDTOTest);
        } else {
            userDTOList.add(userService.findByLogin("test"));
        }

        projectDTOList.add(new ProjectDTO(adminId, "Project 1", "Description 1 admin", Status.IN_PROGRESS));
        projectDTOList.add(new ProjectDTO(adminId, "Project 2", "Description 2 admin", Status.NOT_STARTED));
        projectDTOList.add(new ProjectDTO(userId, "Project 3", "Description 3 user", Status.IN_PROGRESS));
        projectDTOList.add(new ProjectDTO(userId, "Project 4", "Description 4 user", Status.NOT_STARTED));

        taskDTOList.add(new TaskDTO(adminId, "Task 1", "Description 1 admin", Status.NOT_STARTED, projectDTOList.get(0).getId()));
        taskDTOList.add(new TaskDTO(adminId, "Task 2", "Description 2 admin", Status.IN_PROGRESS, projectDTOList.get(1).getId()));
        taskDTOList.add(new TaskDTO(userId, "Task 3", "Description 3 user", Status.IN_PROGRESS, projectDTOList.get(2).getId()));
        taskDTOList.add(new TaskDTO(userId, "Task 4", "Description 4 user", Status.IN_PROGRESS, null));

        projectService.removeAll();
        taskService.removeAll();
        for (@NotNull final ProjectDTO projectDTO : projectDTOList) projectService.add(projectDTO);
        for (@NotNull final TaskDTO taskDTO : taskDTOList) taskService.add(taskDTO);
    }

    @After
    public void clear() {
        projectService.removeAll();
        taskService.removeAll();
        projectDTOList.clear();
        taskDTOList.clear();
        userDTOList.clear();
    }

    @Test
    public void testAdd() {
        @NotNull final TaskDTO taskDTO = new TaskDTO(
                userDTOList.get(0).getId(),
                "Test Task",
                "Test Add Task",
                Status.NOT_STARTED,
                null);
        taskService.add(taskDTO);
        Assert.assertTrue(taskService.existsById(taskDTO.getId()));
    }

    @Test
    public void testAddAll() {
        @NotNull List<TaskDTO> taskDTOS = new ArrayList<>();
        final int taskSize = taskService.getSize();
        for (int i = 0; i < taskSize; i++) {
            taskDTOS.add(new TaskDTO(
                    userDTOList.get(0).getId(),
                    "Task Test Add " + i,
                    "Task Add Desc " + i,
                    Status.NOT_STARTED,
                    projectDTOList.get(0).getId()));
        }
        taskService.addAll(taskDTOS);
        Assert.assertEquals(taskSize * 2, taskService.getSize());

        taskService.removeAll(taskDTOS);
    }

    @Test
    public void testCreate() {
        for (@NotNull final UserDTO userDTO : userDTOList) {
            @NotNull final String userId = userDTO.getId();
            @NotNull final String name = "Task" + userDTO.getLogin();
            @NotNull final String description = "Description_" + userDTO.getLogin();
            @NotNull final TaskDTO taskDTO = taskService.create(name, userId, description);
            Assert.assertNotNull(taskDTO);
            taskService.remove(taskDTO);
        }
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreateWithNullUser() {
        Assert.assertNotNull(taskService.create(
                null,
                "Test Task",
                "Test Description")
        );
    }

    @Test(expected = NameEmptyException.class)
    public void testCreateWithNullName() {
        Assert.assertNotNull(taskService.create(
                userDTOList.get(0).getId(),
                null,
                "Test Description")
        );
    }

    @Test
    public void testChangeStatusByIdPositive() {
        for (@NotNull final TaskDTO taskDTO : taskDTOList) {
            @Nullable final String userId = taskDTO.getUserId();
            @NotNull final String id = taskDTO.getId();
            taskService.changeTaskStatusById(userId, id, Status.COMPLETED);
            @Nullable final TaskDTO actualTaskDTO = taskService.findOneById(id);
            Assert.assertNotNull(actualTaskDTO);
            @NotNull final Status actualStatus = actualTaskDTO.getStatus();
            Assert.assertEquals(Status.COMPLETED, actualStatus);
        }
    }

    @Test(expected = TaskNotFoundException.class)
    public void testChangeStatusByIdNegative() {
        @NotNull final String userId = "Other_user_id";
        @NotNull final String id = "Other_id";
        taskService.changeTaskStatusById(userId, id, Status.COMPLETED);
    }

    @Test
    public void testChangeStatusByIndexPositive() {
        for (@NotNull final UserDTO userDTO : userDTOList) {
            @NotNull final String userId = userDTO.getId();
            @NotNull final List<TaskDTO> userTaskDTOS = taskService.findAll(userId);
            for (int i = 0; i < userTaskDTOS.size(); i++) {
                @Nullable final TaskDTO taskDTO = taskService.findOneByIndex(userId, i);
                Assert.assertNotNull(taskDTO);
                taskService.changeTaskStatusByIndex(userId, i, Status.IN_PROGRESS);
                @NotNull final Status status = taskService.findOneById(taskDTO.getId()).getStatus();
                Assert.assertEquals(Status.IN_PROGRESS, status);
            }
        }
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeStatusByIndexNegative() {
        for (@NotNull final UserDTO userDTO : userDTOList) {
            @NotNull final String userId = userDTO.getId();
            @NotNull final List<TaskDTO> userTaskDTOS = taskService.findAll(userId);
            taskService.changeTaskStatusByIndex(userId, userTaskDTOS.size() + 1, Status.IN_PROGRESS);
        }
    }

    @Test
    public void testExistsByIdPositive() {
        for (@NotNull final TaskDTO taskDTO : taskDTOList) {
            @NotNull final String id = taskDTO.getId();
            Assert.assertTrue(taskService.existsById(id));
        }
    }

    @Test
    public void testExistsByIdNegative() {
        Assert.assertFalse(taskService.existsById("Other_Id"));
        Assert.assertFalse(taskService.existsById("Other_user_id", "Other_Id"));
    }

    @Test
    public void testFindAll() {
        @NotNull final List<TaskDTO> taskDTOS = taskService.findAll();
        Assert.assertNotNull(taskDTOS);
        Assert.assertEquals(taskService.getSize(), taskDTOS.size());
    }

    @Test
    public void testFindAllByUserId() {
        for (@NotNull final UserDTO userDTO : userDTOList) {
            @NotNull final String userId = userDTO.getId();
            @NotNull final List<TaskDTO> actualTaskDTOS = taskService.findAll(userId);
            @NotNull final List<TaskDTO> expectedTaskDTOS = taskDTOList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            Assert.assertEquals(expectedTaskDTOS, actualTaskDTOS);
        }
    }

    @Test
    public void testFindOneByIdPositive() {
        for (@NotNull final TaskDTO taskDTO : taskDTOList) {
            @NotNull final String id = taskDTO.getId();
            Assert.assertEquals(taskDTO.getId(), taskService.findOneById(id).getId());
        }
    }

    @Test
    public void testFindOneByIdNegative() {
        @NotNull final String id = "Other_id";
        @Nullable final TaskDTO taskDTO = taskService.findOneById(id);
        Assert.assertNull(taskDTO);
    }

    @Test
    public void testFindOneByIdUserPositive() {
        for (@NotNull final TaskDTO taskDTO : taskDTOList) {
            @NotNull final String id = taskDTO.getId();
            @Nullable final String userId = taskDTO.getUserId();
            Assert.assertEquals(taskDTO.getId(), taskService.findOneByIdAndUserId(userId, id).getId());
        }
    }

    @Test
    public void testFindOneByIdForUserNegative() {
        @NotNull final String id = "Other_Id";
        @NotNull final String userId = "Other_UserId";
        @Nullable final TaskDTO taskDTO = taskService.findOneByIdAndUserId(userId, id);
        Assert.assertNull(taskDTO);
    }

    @Test
    public void testFindByIndexForUserPositive() {
        for (@NotNull final UserDTO userDTO : userDTOList) {
            @NotNull final String userId = userDTO.getId();
            @NotNull final List<TaskDTO> taskDTOS = taskDTOList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            for (int i = 0; i < taskDTOS.size(); i++) {
                Assert.assertNotNull(taskService.findOneByIndex(userId, i));
                @Nullable final TaskDTO actualTaskDTO = taskService.findOneByIndex(userId, i);
                Assert.assertNotNull(actualTaskDTO);
                @Nullable final TaskDTO expectedTaskDTO = taskDTOS.get(i);
                Assert.assertEquals(expectedTaskDTO, actualTaskDTO);
            }
        }
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIndexForUserNegative() {
        @NotNull final String user_id = "Other_User_Id";
        final int index = taskDTOList.size() + 1;
        Assert.assertNull(taskService.findOneByIndex(user_id, index));
    }

    @Test
    public void testGetSize() {
        final int taskSize = taskService.getSize();
        Assert.assertEquals(taskDTOList.size(), taskSize);
    }

    @Test
    public void testGetSizeForUser() {
        for (@NotNull final UserDTO userDTO : userDTOList) {
            @NotNull final String user_id = userDTO.getId();
            final int actualProjectSize = taskService.getSize(user_id);
            @NotNull final List<TaskDTO> expectedTaskDTOS = taskDTOList.stream()
                    .filter(m -> user_id.equals(m.getUserId()))
                    .collect(Collectors.toList());
            final int expectedTaskSize = expectedTaskDTOS.size();
            Assert.assertEquals(expectedTaskSize, actualProjectSize);
        }
    }

    @Test
    public void testRemoveOne() {
        for (int i = 0; i < taskDTOList.size(); i++) {
            @NotNull final TaskDTO taskDTO = taskDTOList.get(i);
            taskService.removeOne(taskDTO.getUserId(), taskDTO);
            Assert.assertEquals(taskService.getSize(), taskDTOList.size() - i - 1);
        }
        Assert.assertEquals(0, taskService.getSize());
    }

    @Test
    public void testRemoveOneByIdPositive() {
        for (@NotNull final TaskDTO taskDTO : taskDTOList) {
            @NotNull final String id = taskDTO.getId();
            taskService.removeOneById(id);
            Assert.assertNull(taskService.findOneById(id));
        }
    }

    @Test
    public void testRemoveOneByIdNegative() {
        @NotNull final String id = "Other_Id";
        taskService.removeOneById(id);
        Assert.assertEquals(taskDTOList.size(), taskService.getSize());
    }

    @Test
    public void testRemoveAll() {
        taskService.removeAll();
        Assert.assertEquals(0, taskService.getSize());
    }

    @Test
    public void testRemoveAllByUserIdPositive() {
        for (@NotNull final UserDTO userDTO : userDTOList) {
            @NotNull final String user_id = userDTO.getId();
            @Nullable List<TaskDTO> tasksBcp = taskService.findAll(user_id);
            taskService.removeAllByUserId(user_id);
            Assert.assertEquals(0, taskService.getSize(user_id));
            taskService.addAll(tasksBcp);
        }
    }

    @Test
    public void testRemoveAllByUserIdNegative() {
        int expected_size = taskService.getSize();
        taskService.removeAllByUserId("Other_user_id");
        Assert.assertEquals(expected_size, taskService.getSize());
    }

    @Test
    public void testUpdateByIdPositive() {
        @Nullable List<TaskDTO> taskDTOS = taskService.findAll();
        if (taskDTOS == null) {
            taskService.addAll(taskDTOList);
            taskDTOS = taskService.findAll();
        }
        for (@NotNull final TaskDTO taskDTO : taskDTOS) {
            @Nullable final String userId = taskDTO.getUserId();
            @NotNull final String id = taskDTO.getId();
            @NotNull final String name = "Task Test Update";
            @NotNull final String description = "Description Test Update";
            @Nullable final TaskDTO actualTaskDTO = taskService.updateById(userId, id, name, description);
            Assert.assertEquals(name, actualTaskDTO.getName());
            Assert.assertEquals(description, actualTaskDTO.getDescription());

            taskService.remove(taskDTO);
        }
    }

    @Test(expected = TaskNotFoundException.class)
    public void testUpdateByIdNegative() {
        @NotNull final String userId = "Other_User_Id";
        @NotNull final String id = "Other_id";
        @NotNull final String name = "Task Test Update " + id;
        @NotNull final String description = "Description Test Update " + id;
        Assert.assertNotNull(taskService.updateById(userId, id, name, description));
    }

}
