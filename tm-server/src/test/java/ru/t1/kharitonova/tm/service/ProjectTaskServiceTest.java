package ru.t1.kharitonova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.kharitonova.tm.api.service.*;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.kharitonova.tm.exception.entity.TaskNotFoundException;
import ru.t1.kharitonova.tm.exception.field.ProjectIdEmptyException;
import ru.t1.kharitonova.tm.exception.field.TaskIdEmptyException;
import ru.t1.kharitonova.tm.exception.field.UserIdEmptyException;
import ru.t1.kharitonova.tm.dto.model.ProjectDTO;
import ru.t1.kharitonova.tm.dto.model.TaskDTO;
import ru.t1.kharitonova.tm.dto.model.UserDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Ignore
public class ProjectTaskServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    protected final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService, projectService, taskService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService, projectService, taskService);

    @NotNull
    private List<ProjectDTO> projectDTOList;

    @NotNull
    private List<TaskDTO> taskDTOList;

    @NotNull
    private List<UserDTO> userDTOList;

    @NotNull
    private final String userId = UUID.randomUUID().toString();

    @NotNull
    private final String projectName = "Project Name Test";

    @NotNull
    private final String taskName = "Task Name Test";

    @NotNull
    private final String description = "Description";

    @Before
    public void init() {
        projectDTOList = new ArrayList<>();
        taskDTOList = new ArrayList<>();
        userDTOList = new ArrayList<>();

        @NotNull final UserDTO userDTOAdmin = new UserDTO();
        @NotNull final String adminId = userDTOAdmin.getId();
        userDTOAdmin.setLogin("admin");
        userDTOAdmin.setPasswordHash("admin");
        userDTOAdmin.setEmail("admin@test.ru");
        userDTOAdmin.setRole(Role.ADMIN);

        @NotNull final UserDTO userDTOTest = new UserDTO();
        @NotNull final String userId = userDTOTest.getId();
        userDTOTest.setLogin("test");
        userDTOTest.setPasswordHash("test");
        userDTOTest.setEmail("test@test.ru");
        userDTOTest.setRole(Role.USUAL);

        if (userService.findByLogin("admin") == null) {
            userService.add(userDTOAdmin);
            userDTOList.add(userDTOAdmin);
        } else {
            userDTOList.add(userService.findByLogin("admin"));
        }
        if (userService.findByLogin("test") == null) {
            userService.add(userDTOTest);
            userDTOList.add(userDTOTest);
        } else {
            userDTOList.add(userService.findByLogin("test"));
        }

        projectDTOList.add(new ProjectDTO(adminId, "Test Project 1", "Description 1 admin", Status.IN_PROGRESS));
        projectDTOList.add(new ProjectDTO(adminId, "Test Project 2", "Description 2 admin", Status.NOT_STARTED));
        projectDTOList.add(new ProjectDTO(userId, "Test Project 3", "Description 3 user", Status.IN_PROGRESS));
        projectDTOList.add(new ProjectDTO(userId, "Test Project 4", "Description 4 user", Status.NOT_STARTED));

        taskDTOList.add(new TaskDTO(adminId, "Test Task 1", "Description 1 admin", Status.NOT_STARTED, projectDTOList.get(0).getId()));
        taskDTOList.add(new TaskDTO(adminId, "Test Task 2", "Description 2 admin", Status.IN_PROGRESS, projectDTOList.get(1).getId()));
        taskDTOList.add(new TaskDTO(userId, "Test Task 3", "Description 3 user", Status.IN_PROGRESS, projectDTOList.get(2).getId()));
        taskDTOList.add(new TaskDTO(userId, "Test Task 4", "Description 4 user", Status.IN_PROGRESS, null));

        projectService.removeAll();
        taskService.removeAll();
        for (@NotNull final ProjectDTO projectDTO : projectDTOList) projectService.add(projectDTO);
        for (@NotNull final TaskDTO taskDTO : taskDTOList) taskService.add(taskDTO);
    }

    @After
    public void clear() {
        projectService.removeAll();
        taskService.removeAll();
        taskDTOList.clear();
        userDTOList.clear();
    }

    @Test
    public void testBindTaskToProject() {
        @NotNull final ProjectDTO projectDTO = projectService.create(userId, projectName, description);
        @NotNull final String projectId = projectDTO.getId();
        @Nullable TaskDTO taskDTO = taskService.create(userId, taskName, description);
        @NotNull final String taskId = taskDTO.getId();

        projectTaskService.bindTaskToProject(userId, projectId, taskId);
        @Nullable TaskDTO updatedTaskDTO = taskService.findOneById(taskId);
        Assert.assertNotNull(updatedTaskDTO);
        Assert.assertEquals(updatedTaskDTO.getProjectId(), projectId);

        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject("", projectId, taskId)
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(null, projectId, taskId)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(userId, "", taskId)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(userId, null, taskId)
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(userId, projectId, "")
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(userId, projectId, null)
        );
        Assert.assertThrows(
                ProjectNotFoundException.class,
                () -> projectTaskService.bindTaskToProject(userId, UUID.randomUUID().toString(), taskId)
        );
        Assert.assertThrows(
                TaskNotFoundException.class,
                () -> projectTaskService.bindTaskToProject(userId, projectId, UUID.randomUUID().toString())
        );
    }

    @Test
    public void testRemoveProjectById() {
        @NotNull final ProjectDTO projectDTO = projectService.create(userId, projectName, description);
        @NotNull final String projectId = projectDTO.getId();
        @NotNull final TaskDTO taskDTO = taskService.create(userId, taskName, description);
        @NotNull final String taskId = taskDTO.getId();
        projectTaskService.bindTaskToProject(userId, projectId, taskId);
        projectTaskService.removeProjectById(userId, projectId);
        Assert.assertNull(projectService.findOneById(projectId));
        Assert.assertNull(taskService.findOneById(taskId));

        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.removeProjectById("", projectId)
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.removeProjectById(null, projectId)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.removeProjectById(userId, null)
        );
    }

    @Test
    public void testUnbindTaskFromProject() {
        @NotNull final ProjectDTO projectDTO = projectService.create(userId, projectName, description);
        @NotNull final String projectId = projectDTO.getId();
        @NotNull final TaskDTO taskDTO = taskService.create(userId, taskName, description);
        @NotNull final String taskId = taskDTO.getId();
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject("", projectId, taskId)
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject(null, projectId, taskId)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject(userId, "", taskId)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject(userId, null, taskId)
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject(userId, projectId, "")
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject(userId, projectId, null)
        );
        Assert.assertThrows(
                ProjectNotFoundException.class,
                () -> projectTaskService.unbindTaskFromProject(userId, UUID.randomUUID().toString(), taskId)
        );
        Assert.assertThrows(
                TaskNotFoundException.class,
                () -> projectTaskService.unbindTaskFromProject(userId, projectId, UUID.randomUUID().toString())
        );
        projectTaskService.unbindTaskFromProject(userId, projectId, taskId);
        Assert.assertNotNull(taskService.findOneById(taskId));
        Assert.assertNull(taskService.findOneById(taskId).getProjectId());
        Assert.assertNull(taskDTO.getProjectId());
    }

}
