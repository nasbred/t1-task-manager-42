package ru.t1.kharitonova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.kharitonova.tm.api.service.*;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.kharitonova.tm.exception.field.IndexIncorrectException;
import ru.t1.kharitonova.tm.exception.field.NameEmptyException;
import ru.t1.kharitonova.tm.exception.field.UserIdEmptyException;
import ru.t1.kharitonova.tm.dto.model.ProjectDTO;
import ru.t1.kharitonova.tm.dto.model.TaskDTO;
import ru.t1.kharitonova.tm.dto.model.UserDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Ignore
public class ProjectServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    protected final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService, projectService, taskService);

    @NotNull
    private List<ProjectDTO> projectDTOList;

    @NotNull
    private List<TaskDTO> taskDTOList;

    @NotNull
    private List<UserDTO> userDTOList;

    @Before
    public void init() {
        projectDTOList = new ArrayList<>();
        taskDTOList = new ArrayList<>();
        userDTOList = new ArrayList<>();

        @NotNull final UserDTO userDTOAdmin = new UserDTO();
        @NotNull final String adminId = userDTOAdmin.getId();
        userDTOAdmin.setLogin("admin");
        userDTOAdmin.setPasswordHash("admin");
        userDTOAdmin.setEmail("admin@test.ru");
        userDTOAdmin.setRole(Role.ADMIN);

        @NotNull final UserDTO userDTOTest = new UserDTO();
        @NotNull final String userId = userDTOTest.getId();
        userDTOTest.setLogin("test");
        userDTOTest.setPasswordHash("test");
        userDTOTest.setEmail("test@test.ru");
        userDTOTest.setRole(Role.USUAL);

        if (userService.findByLogin("admin") == null) {
            userService.add(userDTOAdmin);
            userDTOList.add(userDTOAdmin);
        } else {
            userDTOList.add(userService.findByLogin("admin"));
        }
        if (userService.findByLogin("test") == null) {
            userService.add(userDTOTest);
            userDTOList.add(userDTOTest);
        } else {
            userDTOList.add(userService.findByLogin("test"));
        }

        projectDTOList.add(new ProjectDTO(adminId, "Test Project 1", "Description 1 admin", Status.IN_PROGRESS));
        projectDTOList.add(new ProjectDTO(adminId, "Test Project 2", "Description 2 admin", Status.NOT_STARTED));
        projectDTOList.add(new ProjectDTO(userId, "Test Project 3", "Description 3 user", Status.IN_PROGRESS));
        projectDTOList.add(new ProjectDTO(userId, "Test Project 4", "Description 4 user", Status.NOT_STARTED));

        taskDTOList.add(new TaskDTO(adminId, "Test Task 1", "Description 1 admin", Status.NOT_STARTED, projectDTOList.get(0).getId()));
        taskDTOList.add(new TaskDTO(adminId, "Test Task 2", "Description 2 admin", Status.IN_PROGRESS, projectDTOList.get(1).getId()));
        taskDTOList.add(new TaskDTO(userId, "Test Task 3", "Description 3 user", Status.IN_PROGRESS, projectDTOList.get(2).getId()));
        taskDTOList.add(new TaskDTO(userId, "Test Task 4", "Description 4 user", Status.IN_PROGRESS, null));

        projectService.removeAll();
        taskService.removeAll();
        for (@NotNull final ProjectDTO projectDTO : projectDTOList) projectService.add(projectDTO);
        for (@NotNull final TaskDTO taskDTO : taskDTOList) taskService.add(taskDTO);
    }

    @After
    public void clear() {
        projectService.removeAll();
        taskService.removeAll();
        taskDTOList.clear();
        userDTOList.clear();
    }

    @Test
    public void testAdd() {
        @NotNull final ProjectDTO projectDTO = new ProjectDTO(
                userDTOList.get(0).getId(),
                "Test Project",
                "Test Add Project",
                Status.NOT_STARTED);
        projectService.add(projectDTO);
        Assert.assertTrue(projectService.existsById(projectDTO.getId()));

        projectService.remove(projectDTO);
    }

    @Test
    public void testAddAll() {
        @NotNull List<ProjectDTO> projectDTOS = new ArrayList<>();
        final int projectSize = projectService.getSize();
        for (int i = 0; i < projectSize; i++) {
            projectDTOS.add(new ProjectDTO(
                    userDTOList.get(0).getId(),
                    "Project Test Add " + i,
                    "Project Add Desc " + i,
                    Status.NOT_STARTED));
        }
        projectService.addAll(projectDTOS);
        Assert.assertEquals(projectSize * 2, projectService.getSize());

        projectService.removeAll(projectDTOS);
    }

    @Test
    public void testCreate() {
        for (@NotNull final UserDTO userDTO : userDTOList) {
            @NotNull final String userId = userDTO.getId();
            @NotNull final String name = "Test_Project_" + userDTO.getLogin();
            @NotNull final String description = "Test_Description_" + userDTO.getLogin();
            @NotNull final ProjectDTO projectDTO = projectService.create(userId, name, description);
            Assert.assertNotNull(projectDTO);
            projectService.remove(projectDTO);
        }
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreateWithNullUser() {
        Assert.assertNotNull(projectService.create(
                null,
                "Test Project",
                "Test Description")
        );
    }

    @Test(expected = NameEmptyException.class)
    public void testCreateWithNullName() {
        Assert.assertNotNull(projectService.create(
                userDTOList.get(0).getId(),
                null,
                "Test Description")
        );
    }

    @Test
    public void testChangeStatusByIdPositive() {
        @Nullable List<ProjectDTO> projectDTOS = projectService.findAll();
        if (projectDTOS == null) {
            projectService.addAll(projectDTOList);
            projectDTOS = projectDTOList;
        }
        for (@NotNull final ProjectDTO projectDTO : projectDTOS) {
            @Nullable final String userId = projectDTO.getUserId();
            @NotNull final String id = projectDTO.getId();
            projectService.changeProjectStatusById(userId, id, Status.COMPLETED);
            @Nullable final ProjectDTO actualProjectDTO = projectService.findOneByIdAndUserId(userId, id);
            Assert.assertNotNull(actualProjectDTO);
            @NotNull final Status actualStatus = actualProjectDTO.getStatus();
            Assert.assertEquals(Status.COMPLETED, actualStatus);
        }
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testChangeStatusByIdNegative() {
        @NotNull final String userId = "Other_user_id";
        @NotNull final String id = "Other_id";
        projectService.changeProjectStatusById(userId, id, Status.COMPLETED);
    }

    @Test
    public void testChangeStatusByIndexPositive() {
        for (@NotNull final UserDTO userDTO : userDTOList) {
            @NotNull final String userId = userDTO.getId();
            @NotNull final List<ProjectDTO> userProjectDTOS = projectService.findAll(userId);
            for (int i = 0; i < userProjectDTOS.size(); i++) {
                @Nullable final ProjectDTO projectDTO = projectService.findOneByIndex(userId, i);
                Assert.assertNotNull(projectDTO);
                projectService.changeProjectStatusByIndex(userId, i, Status.IN_PROGRESS);
                @NotNull final Status status = projectDTO.getStatus();
                Assert.assertEquals(Status.IN_PROGRESS, status);
            }
        }
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeStatusByIndexNegative() {
        for (@NotNull final UserDTO userDTO : userDTOList) {
            @NotNull final String userId = userDTO.getId();
            @NotNull final List<ProjectDTO> userProjectDTOS = projectService.findAll(userId);
            projectService.changeProjectStatusByIndex(userId, userProjectDTOS.size() + 1, Status.IN_PROGRESS);
        }
    }

    @Test
    public void testExistsByIdPositive() {
        for (@NotNull final ProjectDTO projectDTO : projectDTOList) {
            @NotNull final String id = projectDTO.getId();
            Assert.assertTrue(projectService.existsById(id));
        }
    }

    @Test
    public void testExistsByIdNegative() {
        Assert.assertFalse(projectService.existsById("Other_Id"));
        Assert.assertFalse(projectService.existsById("Other_user_id", "Other_Id"));
    }

    @Test
    public void testFindAll() {
        @NotNull final List<ProjectDTO> projectDTOS = projectService.findAll();
        Assert.assertNotNull(projectDTOS);
        Assert.assertEquals(projectDTOList.size(), projectDTOS.size());
    }

    @Test
    public void testFindAllByUserId() {
        for (@NotNull final UserDTO userDTO : userDTOList) {
            @NotNull final String userId = userDTO.getId();
            Assert.assertNotNull(userId);
            @NotNull final List<ProjectDTO> actualProjectDTOS = projectService.findAll(userId);
            @NotNull final List<ProjectDTO> expectedProjectDTOS = projectDTOList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            Assert.assertEquals(expectedProjectDTOS, actualProjectDTOS);
        }
    }

    @Test
    public void testFindOneByIdPositive() {
        for (@NotNull final ProjectDTO projectDTO : projectDTOList) {
            @NotNull final String id = projectDTO.getId();
            Assert.assertEquals(projectDTO, projectService.findOneById(id));
        }
    }

    @Test
    public void testFindOneByIdNegative() {
        @NotNull final String id = "Other_id";
        @Nullable final ProjectDTO projectDTO = projectService.findOneById(id);
        Assert.assertNull(projectDTO);
    }

    @Test
    public void testFindOneByIdUserPositive() {
        for (@NotNull final ProjectDTO projectDTO : projectDTOList) {
            @NotNull final String id = projectDTO.getId();
            @Nullable final String userId = projectDTO.getUserId();
            Assert.assertEquals(projectDTO, projectService.findOneByIdAndUserId(userId, id));
        }
    }

    @Test
    public void testFindOneByIdForUserNegative() {
        @NotNull final String id = "Other_Id";
        @NotNull final String userId = "Other_UserId";
        @Nullable final ProjectDTO projectDTO = projectService.findOneByIdAndUserId(userId, id);
        Assert.assertNull(projectDTO);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindOneByIndexNegative() {
        final int index = projectService.getSize() + 1;
        projectService.findOneByIndex(index);
    }

    @Test
    public void testFindByIndexForUserPositive() {
        for (@NotNull final UserDTO userDTO : userDTOList) {
            @NotNull final String userId = userDTO.getId();
            @NotNull final List<ProjectDTO> projectDTOS = projectDTOList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            for (int i = 0; i < projectDTOS.size(); i++) {
                Assert.assertNotNull(projectService.findOneByIndex(userId, i));
                @Nullable final ProjectDTO actualProjectDTO = projectService.findOneByIndex(userId, i);
                Assert.assertNotNull(actualProjectDTO);
                @Nullable final ProjectDTO expectedProjectDTO = projectDTOS.get(i);
                Assert.assertEquals(expectedProjectDTO, actualProjectDTO);
            }
        }
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIndexForUserNegative() {
        @NotNull final String userId = "Other_User_Id";
        final int index = projectDTOList.size() + 1;
        Assert.assertNull(projectService.findOneByIndex(userId, index));
    }

    @Test
    public void testGetSize() {
        final int projectSize = projectService.getSize();
        Assert.assertEquals(projectDTOList.size(), projectSize);
    }

    @Test
    public void testGetSizeForUser() {
        for (@NotNull final UserDTO userDTO : userDTOList) {
            @NotNull final String userId = userDTO.getId();
            final int actualProjectSize = projectService.getSize(userId);
            @NotNull final List<ProjectDTO> expectedProjectDTOS = projectDTOList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            final int expectedProjectSize = expectedProjectDTOS.size();
            Assert.assertEquals(expectedProjectSize, actualProjectSize);
        }
    }

    @Test
    public void testUpdateByIdPositive() {
        for (@NotNull final ProjectDTO projectDTO : projectDTOList) {
            @Nullable final String userId = projectDTO.getUserId();
            Assert.assertNotNull(userId);
            @NotNull final String id = projectDTO.getId();
            @NotNull final String name = "Project Update";
            @NotNull final String description = "Description Update";
            @Nullable final ProjectDTO actualProjectDTO = projectService.updateById(userId, id, name, description);
            Assert.assertEquals(name, actualProjectDTO.getName());
            Assert.assertEquals(description, actualProjectDTO.getDescription());
        }
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testUpdateByIdNegative() {
        @NotNull final String userId = "Other_User_Id";
        @NotNull final String id = "Other_id";
        @NotNull final String name = "Project Update";
        @NotNull final String description = "Test Update";
        Assert.assertNotNull(projectService.updateById(userId, id, name, description));
    }

    @Test
    public void testUpdateByIndexPositive() {
        for (@NotNull final UserDTO userDTO : userDTOList) {
            @NotNull final String userId = userDTO.getId();
            @NotNull final List<ProjectDTO> projectDTOS = projectDTOList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            for (int i = 0; i < projectDTOS.size(); i++) {
                @NotNull final String name = "Project Test Update " + i;
                @NotNull final String description = "Description Test Update " + i;
                Assert.assertNotNull(projectService.findOneByIndex(userId, i));
                @Nullable final ProjectDTO actualProjectDTO = projectService.updateByIndex(userId, i, name, description);
                Assert.assertEquals(name, actualProjectDTO.getName());
                Assert.assertEquals(description, actualProjectDTO.getDescription());
                Assert.assertEquals(projectDTOS.get(i), actualProjectDTO);
            }
        }
    }

    @Test
    public void testRemoveOne() {
        for (int i = 0; i < projectService.getSize(); i++) {
            @Nullable final ProjectDTO projectDTO = projectService.findOneByIndex(i);
            Assert.assertNotNull(projectDTO);
            projectService.remove(projectDTO);
            Assert.assertNull(projectService.findOneById(projectDTO.getId()));
        }
    }

    @Test
    public void testRemoveOneByIdPositive() {
        for (@NotNull final ProjectDTO projectDTO : projectDTOList) {
            @NotNull final String id = projectDTO.getId();
            projectService.removeOneById(id);
            Assert.assertNull(projectService.findOneById(id));
        }
    }

    @Test
    public void testRemoveOneByIdNegative() {
        @NotNull final String id = "Other_Id";
        projectService.removeOneById(id);
        Assert.assertNull(projectService.findOneById(id));
        Assert.assertEquals(projectDTOList.size(), projectService.getSize());
    }

    @Test
    public void testRemoveAll() {
        @Nullable List<ProjectDTO> projectsBcp = projectService.findAll();
        projectService.removeAll();
        Assert.assertEquals(0, projectService.getSize());
        if (projectsBcp != null) projectService.addAll(projectsBcp);
    }

    @Test
    public void testRemoveAllByUserIdPositive() {
        for (@NotNull final UserDTO userDTO : userDTOList) {
            @NotNull final String userId = userDTO.getId();
            @Nullable List<ProjectDTO> projectsBcp = projectService.findAll(userId);
            projectService.removeAllByUserId(userId);
            Assert.assertEquals(0, projectService.getSize(userId));
            projectService.addAll(projectsBcp);
        }
    }

}
