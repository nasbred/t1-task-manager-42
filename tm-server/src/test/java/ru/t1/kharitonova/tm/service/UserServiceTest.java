package ru.t1.kharitonova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.kharitonova.tm.api.service.*;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.exception.entity.ModelNotFoundException;
import ru.t1.kharitonova.tm.exception.field.IdEmptyException;
import ru.t1.kharitonova.tm.exception.user.EmailEmptyException;
import ru.t1.kharitonova.tm.exception.user.LoginEmptyException;
import ru.t1.kharitonova.tm.exception.user.PasswordEmptyException;
import ru.t1.kharitonova.tm.exception.user.UserNotFoundException;
import ru.t1.kharitonova.tm.dto.model.UserDTO;
import ru.t1.kharitonova.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

@Ignore
public class UserServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService, projectService, taskService);

    @NotNull
    private List<UserDTO> userDTOList = new ArrayList<>();

    @Before
    public void init() {
        @NotNull final UserDTO userDTOAdmin = new UserDTO();
        userDTOAdmin.setLogin("admin");
        userDTOAdmin.setPasswordHash("admin");
        userDTOAdmin.setEmail("admin@test.ru");
        userDTOAdmin.setRole(Role.ADMIN);

        @NotNull final UserDTO userDTOTest = new UserDTO();
        userDTOTest.setLogin("test");
        userDTOTest.setPasswordHash("test");
        userDTOTest.setEmail("test@test.ru");
        userDTOTest.setLocked(true);
        userDTOTest.setRole(Role.USUAL);

        if (userService.findByLogin("admin") == null) {
            userService.add(userDTOAdmin);
            userDTOList.add(userDTOAdmin);
        } else {
            userDTOList.add(userService.findByLogin("admin"));
        }
        if (userService.findByLogin("test") == null) {
            userService.add(userDTOTest);
            userDTOList.add(userDTOTest);
        } else {
            userDTOList.add(userService.findByLogin("test"));
        }
    }

    @After
    public void clear() {
        userService.removeAll();
        userDTOList.clear();
    }

    @Test
    public void testAdd() {
        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setLogin("test_add");
        userDTO.setPasswordHash("test_add");
        userDTO.setEmail("test_add@test.ru");
        userDTO.setRole(Role.USUAL);
        userService.add(userDTO);
        Assert.assertEquals(userDTOList.size() + 1, userService.getSize());
    }

    @Test
    public void testAddAll() {
        @NotNull List<UserDTO> userDTOS = new ArrayList<>();
        final int count = userDTOList.size();
        for (int i = 0; i < count; i++) {
            @NotNull final UserDTO userDTO = new UserDTO();
            userDTO.setLogin("test_add_all " + i);
            userDTO.setPasswordHash("test_add_all " + i);
            userDTO.setEmail("test_add@test.ru");
            userDTO.setRole(Role.USUAL);
            userDTOS.add(userDTO);
        }
        userService.addAll(userDTOS);
        Assert.assertEquals(count * 2, userService.getSize());
    }

    @Test
    public void testCreate() {
        @NotNull final String login = "test_create";
        @NotNull final String password = "test_create";
        @NotNull final Role role = Role.USUAL;

        Assert.assertNotNull(userService.create(login, password, role));
        Assert.assertEquals(userDTOList.size() + 1, userService.getSize());
    }

    @Test(expected = LoginEmptyException.class)
    public void testCreateWithNullLogin() {
        userService.create(null, "password");
    }

    @Test(expected = PasswordEmptyException.class)
    public void testCreateWithNullPassword() {
        userService.create("login", null);
    }

    @Test
    public void testFindAll() {
        @Nullable List<UserDTO> userDTOList = userService.findAll();
        Assert.assertNotNull(userDTOList);
        Assert.assertEquals(userService.getSize(), userDTOList.size());
    }

    @Test
    public void testFindOneById() {
        for (@NotNull final UserDTO userDTO : userDTOList) {
            @NotNull final String id = userDTO.getId();
            @Nullable final UserDTO actualUserDTO = userService.findOneById(id);
            Assert.assertNotNull(actualUserDTO);
            Assert.assertEquals(userDTO.getId(), actualUserDTO.getId());
        }
        Assert.assertNull(userService.findOneById("otherId"));
        Assert.assertThrows(IdEmptyException.class, () -> userService.findOneById(""));
    }

    @Test
    public void testFindOneByLogin() {
        for (@NotNull final UserDTO userDTO : userDTOList) {
            @Nullable final String login = userDTO.getLogin();
            Assert.assertNotNull(login);
            @Nullable final UserDTO actualUserDTO = userService.findByLogin(login);
            Assert.assertNotNull(actualUserDTO);
            Assert.assertEquals(userDTO, actualUserDTO);
        }
        Assert.assertNull(userService.findByLogin("other_login"));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(""));
    }

    @Test
    public void testFindOneByEmail() {
        @Nullable final UserDTO userDTO = userService.findByLogin("test");
        Assert.assertNotNull(userDTO);
        @Nullable final String email = userDTO.getEmail();
        Assert.assertNotNull(email);
        @Nullable final UserDTO actualUserDTO = userService.findByEmail(email);
        Assert.assertNotNull(actualUserDTO);
        Assert.assertEquals(userDTO.getId(), actualUserDTO.getId());

        Assert.assertNull(userService.findByEmail("other_email"));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.findByEmail(""));
    }

    @Test
    public void testGetSize() {
        final int userSize = userService.getSize();
        Assert.assertEquals(userDTOList.size(), userSize);
    }

    @Test
    public void testRemoveOne() {
        for (int i = 0; i < userDTOList.size(); i++) {
            @NotNull final UserDTO userDTO = userDTOList.get(i);
            userService.remove(userDTO);
            Assert.assertEquals(userService.getSize(), userDTOList.size() - i - 1);
        }
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    public void testRemoveOneByIdPositive() {
        for (@NotNull final UserDTO userDTO : userDTOList) {
            @NotNull final String id = userDTO.getId();
            userService.removeOneById(id);
        }
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    public void testRemoveOneByIdNegative() {
        @NotNull final String id = "Other_Id";
        Assert.assertThrows(UserNotFoundException.class, () -> userService.removeOneById(id));
        Assert.assertEquals(userDTOList.size(), userService.getSize());
    }

    @Test
    public void testRemoveByLoginPositive() {
        for (@NotNull final UserDTO userDTO : userDTOList) {
            @Nullable final String login = userDTO.getLogin();
            Assert.assertNotNull(userService.removeByLogin(login));
        }
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    public void testRemoveByLoginNegative() {
        @NotNull final String login = "Other_Login";
        Assert.assertThrows(UserNotFoundException.class, () -> userService.removeOneById(login));
        Assert.assertEquals(userDTOList.size(), userService.getSize());
    }

    @Test
    public void testRemoveByEmailPositive() {
        for (@NotNull final UserDTO userDTO : userDTOList) {
            @Nullable final String email = userDTO.getEmail();
            Assert.assertNotNull(userService.removeByEmail(email));
        }
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    public void testRemoveByEmailNegative() {
        @NotNull final String email = "Other_Email";
        Assert.assertThrows(ModelNotFoundException.class, () -> userService.removeByEmail(email));
        Assert.assertEquals(userDTOList.size(), userService.getSize());
    }

    @Test
    public void testRemoveAll() {
        taskService.removeAll();
        Assert.assertEquals(0, taskService.getSize());
    }

    @Test
    public void testSetPassword() {
        @NotNull final String newPassword = "newPassword";
        for (@NotNull final UserDTO userDTO : userDTOList) {
            @NotNull final IPropertyService propertyService = new PropertyService();
            @NotNull final String id = userDTO.getId();
            userService.setPassword(id, newPassword);
            @Nullable final UserDTO actualUserDTO = userService.findOneById(id);

            Assert.assertNotNull(actualUserDTO);
            Assert.assertEquals(HashUtil.salt(propertyService, newPassword), actualUserDTO.getPasswordHash());
            Assert.assertThrows(
                    PasswordEmptyException.class, () -> userService.setPassword(id, "")
            );
            Assert.assertThrows(
                    PasswordEmptyException.class, () -> userService.setPassword(id, null)
            );
        }

        Assert.assertThrows(
                IdEmptyException.class,
                () -> userService.setPassword(null, newPassword)
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> userService.setPassword("", newPassword)
        );
        Assert.assertThrows(
                UserNotFoundException.class,
                () -> userService.setPassword("other_Id", newPassword)
        );
    }

    @Test
    public void testUpdateUser() {
        @NotNull final String firstName = "Update First Name";
        @NotNull final String lastName = "Update Last Name";
        @NotNull final String middleName = "Update Middle Name";
        for (@NotNull final UserDTO userDTO : userDTOList) {
            @NotNull final String id = userDTO.getId();
            @NotNull final UserDTO actualUserDTO = userService.updateUser(id, firstName, lastName, middleName);
            Assert.assertEquals(firstName, actualUserDTO.getFirstName());
            Assert.assertEquals(lastName, actualUserDTO.getLastName());
            Assert.assertEquals(middleName, actualUserDTO.getMiddleName());
        }
        Assert.assertEquals(userDTOList.size(), userService.getSize());

        Assert.assertThrows(
                IdEmptyException.class,
                () -> userService.updateUser(null, firstName, lastName, middleName)
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> userService.updateUser("", firstName, lastName, middleName)
        );
        Assert.assertThrows(
                UserNotFoundException.class,
                () -> userService.updateUser("otherId", firstName, lastName, middleName)
        );
    }

    @Test
    public void testLockUserByLogin() {
        for (@NotNull final UserDTO userDTO : userDTOList) {
            @Nullable final String login = userDTO.getLogin();
            @Nullable final UserDTO actualUserDTO = userService.lockUserByLogin(login);
            Assert.assertNotNull(actualUserDTO);
            Assert.assertTrue(actualUserDTO.isLocked());
        }
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(""));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.lockUserByLogin("random_login"));
    }


    @Test
    public void testUnlockUserByLogin() {
        for (@NotNull final UserDTO userDTO : userDTOList) {
            @Nullable final String login = userDTO.getLogin();
            @Nullable final UserDTO actualUserDTO = userService.unlockUserByLogin(login);
            Assert.assertNotNull(actualUserDTO);
            Assert.assertFalse(actualUserDTO.isLocked());
        }
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unlockUserByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unlockUserByLogin(""));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.unlockUserByLogin("otherLogin"));
    }

}
