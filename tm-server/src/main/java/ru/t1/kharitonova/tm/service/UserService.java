package ru.t1.kharitonova.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.repository.IUserRepository;
import ru.t1.kharitonova.tm.api.service.*;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.exception.entity.ModelNotFoundException;
import ru.t1.kharitonova.tm.exception.field.IdEmptyException;
import ru.t1.kharitonova.tm.exception.field.IndexIncorrectException;
import ru.t1.kharitonova.tm.exception.user.*;
import ru.t1.kharitonova.tm.dto.model.UserDTO;
import ru.t1.kharitonova.tm.util.HashUtil;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public class UserService implements IUserService {

    @NotNull
    private final IConnectionService connectionService;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    public UserService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IPropertyService propertyService,
            @NotNull final IProjectService projectService,
            @NotNull final ITaskService taskService
    ) {
        this.connectionService = connectionService;
        this.propertyService = propertyService;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO add(@Nullable final UserDTO model) {
        if (model == null) throw new ModelNotFoundException();
        @Nullable final UserDTO result;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            repository.add(model);
            result = model;
            sqlSession.commit();
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @NotNull
    @SneakyThrows
    public Collection<UserDTO> addAll(@NotNull final Collection<UserDTO> models) {
        @Nullable final Collection<UserDTO> result;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            result = repository.addAll(models);
            sqlSession.commit();
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setLogin(login);
        userDTO.setPasswordHash(HashUtil.salt(propertyService, password));
        userDTO.setRole(Role.USUAL);
        try {
            repository.add(userDTO);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return userDTO;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setLogin(login);
        userDTO.setPasswordHash(HashUtil.salt(propertyService, password));
        userDTO.setRole(Role.USUAL);
        userDTO.setEmail(email);
        try {
            repository.add(userDTO);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return userDTO;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setLogin(login);
        userDTO.setPasswordHash(HashUtil.salt(propertyService, password));
        if (role != null) userDTO.setRole(role);
        try {
            repository.add(userDTO);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return userDTO;
    }

    @Override
    @SneakyThrows
    public int getSize() {
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            return repository.getSize();
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            return repository.existsById(id);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            return repository.findOneById(id);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            return repository.findOneByIndex(index);
        }
    }

    @Nullable
    @SneakyThrows
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            return repository.findByLogin(login);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            return repository.findByEmail(email);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<UserDTO> findAll() {
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            return repository.findAll();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<UserDTO> findAll(@Nullable final Comparator<UserDTO> comparator) {
        if (comparator == null) return findAll();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            return repository.findAllWithComparator(comparator);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO remove(@Nullable final UserDTO model) {
        if (model == null) throw new ModelNotFoundException();
        @Nullable final UserDTO userDTO;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            userDTO = repository.remove(model);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return userDTO;
    }

    @SneakyThrows
    public void removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            if (repository.findOneById(id) == null) throw new UserNotFoundException();
            repository.removeOneById(id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO removeOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        @Nullable UserDTO record;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            record = repository.removeOneByIndex(index);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return record;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        @Nullable final UserDTO userDTO;
        try {
            userDTO = repository.findByLogin(login);
            remove(userDTO);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return userDTO;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        @Nullable final UserDTO userDTO;
        try {
            userDTO = repository.findByEmail(email);
            remove(userDTO);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return userDTO;
    }

    @Override
    @SneakyThrows
    public void removeAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            repository.removeAll();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    public void removeAll(@Nullable final List<UserDTO> models) {
        if (models == null || models.isEmpty()) return;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.removeAll(models);
        }
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<UserDTO> set(@NotNull final Collection<UserDTO> models) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        @Nullable final Collection<UserDTO> result;
        try {
            result = repository.set(models);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @Override
    public void setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDTO userDTO = findOneById(id);
        if (userDTO == null) throw new UserNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            @Nullable final String password_hash = HashUtil.salt(propertyService, password);
            if (password_hash == null) throw new PasswordEmptyException();
            repository.setPassword(id, password_hash);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public UserDTO updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final UserDTO userDTO = findOneById(id);
        if (userDTO == null) throw new UserNotFoundException();
        userDTO.setFirstName(firstName);
        userDTO.setLastName(lastName);
        userDTO.setMiddleName(middleName);
        update(userDTO);
        return userDTO;
    }

    @Override
    public UserDTO lockUserByLogin(@Nullable final String login) {
        if (login == null) throw new LoginEmptyException();
        @Nullable final UserDTO userDTO = findByLogin(login);
        if (userDTO == null) throw new UserNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            repository.lockUserById(userDTO.getId());
            sqlSession.commit();
            return repository.findOneById(userDTO.getId());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public UserDTO unlockUserByLogin(@Nullable final String login) {
        if (login == null) throw new LoginEmptyException();
        @Nullable final UserDTO userDTO = findByLogin(login);
        if (userDTO == null) throw new UserNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            repository.unLockUserById(userDTO.getId());
            sqlSession.commit();
            return repository.findOneById(userDTO.getId());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void update(@NotNull final UserDTO model) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            repository.updateUser(model.getId(), model.getFirstName(), model.getLastName(), model.getMiddleName());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
