package ru.t1.kharitonova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.exception.user.UserNotFoundException;
import ru.t1.kharitonova.tm.dto.model.UserDTO;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IUserRepository {

    @Insert("INSERT INTO tm_user (id, login, password_hash, email, last_name, first_name, middle_name, role, locked) " +
            "VALUES(#{id}, #{login}, #{passwordHash}, #{email}, #{lastName}, #{firstName}, #{middleName}, " +
            "#{role}, #{locked});")
    void add(@NotNull UserDTO userDTO);

    default Collection<UserDTO> addAll(@NotNull final Collection<UserDTO> userDTOS) {
        @NotNull List<UserDTO> result = new ArrayList<>();
        for (@NotNull final UserDTO userDTO : userDTOS) {
            add(userDTO);
            result.add(userDTO);
        }
        return result;
    }

    default boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Select("SELECT COUNT(1) FROM tm_user;")
    int getSize();

    default UserDTO remove(@Nullable final UserDTO model) {
        if (model == null) throw new UserNotFoundException();
        removeOneById(model.getId());
        return model;
    }

    @Delete("DELETE FROM tm_user WHERE id = #{id};")
    void removeOneById(@NotNull @Param("id") String id);

    default UserDTO removeOneByIndex(@NotNull final Integer index) {
        @Nullable final UserDTO userDTO = findOneByIndex(index);
        if (userDTO == null) return null;
        remove(userDTO);
        return userDTO;
    }

    @Delete("TRUNCATE TABLE tm_user;")
    void removeAll();

    default void removeAll(@NotNull List<UserDTO> userDTOS) {
        for (@NotNull final UserDTO userDTO : userDTOS) remove(userDTO);
    }

    @Select("SELECT * FROM tm_user;")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name")})
    @NotNull
    List<UserDTO> findAll();

    @Select("SELECT * FROM tm_user ORDER BY #{getSortType(comparator)};")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name")})
    @NotNull
    List<UserDTO> findAllWithComparator(@NotNull @Param("comparator") Comparator<UserDTO> comparator);

    @Select("SELECT * FROM tm_user WHERE id = #{id} LIMIT 1;")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name")})
    @Nullable
    UserDTO findOneById(@NotNull @Param("id") String id);

    @Select("SELECT * FROM tm_user OFFSET #{index};")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name")})
    UserDTO findOneByIndex(@Param("index") @NotNull Integer index);

    @Select("SELECT * FROM tm_user WHERE login = #{login} LIMIT 1;")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name")})
    @Nullable
    UserDTO findByLogin(@NotNull @Param("login") String login);

    @Select("SELECT * FROM tm_user WHERE email = #{email} LIMIT 1;")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name")})
    @Nullable
    UserDTO findByEmail(@NotNull @Param("email") String email);

    default Collection<UserDTO> set(@NotNull final Collection<UserDTO> userDTOS) {
        removeAll();
        return addAll(userDTOS);
    }

    @Update("UPDATE tm_user SET password_hash = #{passwordHash} WHERE id = #{id};")
    void setPassword(@Nullable @Param("id") String id, @NotNull @Param("passwordHash") String password);

    @Update("UPDATE tm_user SET last_name = #{lastName}, first_name = #{firstName}, middle_name = #{middleName} " +
            "WHERE id = #{id};")
    void updateUser(@Nullable @Param("id") String id,
                    @Nullable @Param("firstName") String firstName,
                    @Nullable @Param("lastName") String lastName,
                    @Nullable @Param("middleName") String middleName);

    @Update("UPDATE tm_user SET locked = true WHERE id = #{id};")
    void lockUserById(@Nullable @Param("id") String id);

    @Update("UPDATE tm_user SET locked = false WHERE id = #{id};")
    void unLockUserById(@Nullable @Param("id") String id);

}
