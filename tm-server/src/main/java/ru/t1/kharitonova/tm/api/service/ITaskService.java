package ru.t1.kharitonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.dto.model.TaskDTO;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    void add(@NotNull TaskDTO model);

    void add(@Nullable String userId, @NotNull TaskDTO model);

    @NotNull
    Collection<TaskDTO> addAll(@NotNull Collection<TaskDTO> models);

    @NotNull
    TaskDTO changeTaskStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    TaskDTO changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Collection<TaskDTO> set(@NotNull Collection<TaskDTO> models);

    @Nullable
    TaskDTO findOneById(@NotNull String id);

    @Nullable
    TaskDTO findOneByIdAndUserId(@NotNull String user_id, @NotNull String id);

    @Nullable
    TaskDTO findOneByIndex(@NotNull Integer index);

    @Nullable
    TaskDTO findOneByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    List<TaskDTO> findAll();

    @Nullable
    List<TaskDTO> findAll(@NotNull Comparator<TaskDTO> comparator);

    @NotNull
    List<TaskDTO> findAll(@NotNull String userId);

    @NotNull
    List<TaskDTO> findAll(@NotNull String userId, @Nullable Comparator<TaskDTO> comparator);

    @NotNull
    List<TaskDTO> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    int getSize();

    int getSize(@NotNull String userId);

    boolean existsById(@NotNull String id);

    boolean existsById(@NotNull String userId, @NotNull String id);

    @Nullable
    TaskDTO remove(@Nullable TaskDTO model);

    @Nullable
    TaskDTO removeOne(@Nullable String userId, @Nullable TaskDTO model);

    void removeOneById(@NotNull String id);

    @Nullable
    TaskDTO removeOneById(@Nullable String userId, @Nullable String id);

    void removeOneByIndex(@NotNull Integer index);

    TaskDTO removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    void removeAll();

    void removeAll(@NotNull List<TaskDTO> models);

    void removeAllByUserId(@NotNull String userId);

    @Nullable
    TaskDTO update(@NotNull TaskDTO model);

    @NotNull
    TaskDTO updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    TaskDTO updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

}
