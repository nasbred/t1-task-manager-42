package ru.t1.kharitonova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.endpoint.ITaskEndpoint;
import ru.t1.kharitonova.tm.api.service.IProjectTaskService;
import ru.t1.kharitonova.tm.api.service.IServiceLocator;
import ru.t1.kharitonova.tm.api.service.ITaskService;
import ru.t1.kharitonova.tm.dto.request.task.*;
import ru.t1.kharitonova.tm.dto.response.task.*;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.enumerated.TaskSort;
import ru.t1.kharitonova.tm.exception.field.IdEmptyException;
import ru.t1.kharitonova.tm.dto.model.SessionDTO;
import ru.t1.kharitonova.tm.dto.model.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Comparator;
import java.util.List;

@WebService(endpointInterface = "ru.t1.kharitonova.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @NotNull
    private ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskChangeStatusByIdResponse changeStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskChangeStatusByIdRequest request
    ) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final String userId = sessionDTO.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Status status = request.getStatus();
        @Nullable final TaskDTO taskDTO = getTaskService().changeTaskStatusById(userId, id, status);
        return new TaskChangeStatusByIdResponse(taskDTO);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskChangeStatusByIndexResponse changeStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskChangeStatusByIndexRequest request
    ) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = sessionDTO.getUserId();
        @Nullable final Status status = request.getStatus();
        @Nullable final TaskDTO taskDTO = getTaskService().changeTaskStatusByIndex(userId, index, status);
        return new TaskChangeStatusByIndexResponse(taskDTO);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskClearResponse clearTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskClearRequest request
    ) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final String userId = sessionDTO.getUserId();
        getTaskService().removeAllByUserId(userId);
        return new TaskClearResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCreateResponse createTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCreateRequest request
    ) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final String userId = sessionDTO.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final TaskDTO taskDTO = getTaskService().create(userId, name, description);
        return new TaskCreateResponse(taskDTO);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskListResponse listTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskListRequest request
    ) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final String userId = sessionDTO.getUserId();
        @Nullable final TaskSort sort = request.getSort();
        @Nullable final Comparator<TaskDTO> comparator = sort == null ? null : sort.getComparator();
        @NotNull final List<TaskDTO> taskDTOS = getTaskService().findAll(userId, comparator);
        return new TaskListResponse(taskDTOS);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskListByProjectIdResponse listTaskByProjectId(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskListByProjectIdRequest request
    ) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final String userId = sessionDTO.getUserId();
        @Nullable final String id = request.getProjectId();
        @NotNull final List<TaskDTO> taskDTOS = getTaskService().findAllByProjectId(userId, id);
        return new TaskListByProjectIdResponse(taskDTOS);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskShowByIdResponse showTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskShowByIdRequest request
    ) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final String userId = sessionDTO.getUserId();
        @Nullable final String id = request.getId();
        if (id == null) throw new IdEmptyException();
        @Nullable final TaskDTO taskDTO = getTaskService().findOneById(id);
        return new TaskShowByIdResponse(taskDTO);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskShowByIndexResponse showTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskShowByIndexRequest request
    ) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final String userId = sessionDTO.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final TaskDTO taskDTO = getTaskService().findOneByIndex(userId, index);
        return new TaskShowByIndexResponse(taskDTO);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskRemoveByIdResponse removeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskRemoveByIdRequest request
    ) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final String userId = sessionDTO.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final TaskDTO taskDTO = getTaskService().removeOneById(userId, id);
        return new TaskRemoveByIdResponse(taskDTO);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskRemoveByIndexResponse removeTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskRemoveByIndexRequest request
    ) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final String userId = sessionDTO.getUserId();
        @Nullable final Integer index = request.getIndex();
        if (index == null) return new TaskRemoveByIndexResponse(null);
        @Nullable final TaskDTO taskDTO = getTaskService().removeOneByIndex(userId, index);
        return new TaskRemoveByIndexResponse(taskDTO);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUpdateByIdResponse updateTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUpdateByIdRequest request
    ) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final String userId = sessionDTO.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final TaskDTO taskDTO = getTaskService().updateById(userId, id, name, description);
        return new TaskUpdateByIdResponse(taskDTO);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUpdateByIndexResponse updateTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUpdateByIndexRequest request
    ) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final String userId = sessionDTO.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final TaskDTO taskDTO = getTaskService().updateByIndex(userId, index, name, description);
        return new TaskUpdateByIndexResponse(taskDTO);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskBindToProjectResponse bindTaskToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskBindToProjectRequest request
    ) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final String userId = sessionDTO.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
        return new TaskBindToProjectResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUnbindFromProjectResponse unbindTaskToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUnbindFromProjectRequest request
    ) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final String userId = sessionDTO.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);
        return new TaskUnbindFromProjectResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskStartByIdResponse startTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskStartByIdRequest request
    ) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final String userId = sessionDTO.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Status status = Status.IN_PROGRESS;
        @Nullable final TaskDTO taskDTO = getTaskService().changeTaskStatusById(userId, id, status);
        return new TaskStartByIdResponse(taskDTO);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskStartByIndexResponse startTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskStartByIndexRequest request
    ) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = sessionDTO.getUserId();
        @Nullable final Status status = Status.IN_PROGRESS;
        @Nullable final TaskDTO taskDTO = getTaskService().changeTaskStatusByIndex(userId, index, status);
        return new TaskStartByIndexResponse(taskDTO);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCompleteByIdResponse completeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCompleteByIdRequest request
    ) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final String userId = sessionDTO.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Status status = Status.COMPLETED;
        @Nullable final TaskDTO taskDTO = getTaskService().changeTaskStatusById(userId, id, status);
        return new TaskCompleteByIdResponse(taskDTO);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCompleteByIndexResponse completeTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCompleteByIndexRequest request
    ) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = sessionDTO.getUserId();
        @Nullable final Status status = Status.COMPLETED;
        @Nullable final TaskDTO taskDTO = getTaskService().changeTaskStatusByIndex(userId, index, status);
        return new TaskCompleteByIndexResponse(taskDTO);
    }

}
