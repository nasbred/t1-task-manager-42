package ru.t1.kharitonova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.exception.entity.TaskNotFoundException;
import ru.t1.kharitonova.tm.dto.model.TaskDTO;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public interface ITaskRepository {

    @Insert("INSERT INTO tm_task (id, name, description, status, created, user_id, project_id) " +
            "VALUES(#{id}, #{name}, #{description}, #{status}, #{created}, #{userId}, #{projectId});")
    void add(@NotNull TaskDTO taskDTO);

    default Collection<TaskDTO> addAll(@NotNull final Collection<TaskDTO> models) {
        @NotNull List<TaskDTO> result = new ArrayList<>();
        for (@NotNull final TaskDTO model : models) {
            add(model);
            result.add(model);
        }
        return result;
    }

    @Update("UPDATE tm_task SET project_id = #{projectId} WHERE id = #{taskId};")
    void bindTaskToProject(@Nullable @Param("projectId") String projectId, @Nullable @Param("taskId") String taskId);

    default boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Select("SELECT * FROM tm_task;")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")})
    @NotNull
    List<TaskDTO> findAll();

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId};")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")})
    @NotNull
    List<TaskDTO> findAllByUserId(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY #{sort};")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")})
    @NotNull
    List<TaskDTO> findAllByUserIdWithSort(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("sort") String sort
    );

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} AND project_id = #{projectId};")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")})
    @NotNull
    List<TaskDTO> findAllByProjectId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("projectId") String projectId
    );

    @Select("SELECT * FROM tm_task WHERE id = #{id} LIMIT 1;")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")})
    @Nullable
    TaskDTO findOneById(@Param("id") @NotNull String id);

    @Select("SELECT * FROM tm_task WHERE id = #{id} AND user_id = #{user_id} LIMIT 1;")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")})
    @Nullable
    TaskDTO findOneByIdAndUserId(@Param("user_id") @NotNull String user_id, @Param("id") @NotNull String id);

    @Select("SELECT * FROM tm_task OFFSET #{index} LIMIT 1;")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")})
    @Nullable
    TaskDTO findOneByIndex(@Param("index") @NotNull Integer index);

    @Select("SELECT * FROM tm_task WHERE user_id = #{user_id} OFFSET #{index} LIMIT 1;")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")})
    @Nullable
    TaskDTO findOneByIndexByUserId(
            @NotNull @Param("user_id") String user_id,
            @NotNull @Param("index") Integer index
    );

    @Select("SELECT COUNT(1) FROM tm_task;")
    int getSize();

    @Select("SELECT COUNT(1) FROM tm_task WHERE user_id = #{user_id};")
    int getSizeForUser(@NotNull @Param("user_id") String userId);

    default TaskDTO remove(@Nullable final TaskDTO model) {
        if (model == null) throw new TaskNotFoundException();
        removeOneById(model.getId());
        return model;
    }

    @Delete("DELETE FROM tm_task WHERE id = #{id};")
    void removeOneById(@NotNull @Param("id") String id);

    default TaskDTO removeOneByIndex(@NotNull final Integer index) {
        @Nullable final TaskDTO model = findOneByIndex(index);
        if (model == null) return null;
        remove(model);
        return model;
    }

    default TaskDTO removeOneByIndexByUser(@NotNull String userId, @NotNull final Integer index) {
        @Nullable final TaskDTO model = findOneByIndexByUserId(userId, index);
        if (model == null) return null;
        remove(model);
        return model;
    }

    @Delete("TRUNCATE TABLE tm_task;")
    void removeAll();

    default void removeAllByList(@NotNull List<TaskDTO> taskDTOS) {
        for (@NotNull final TaskDTO taskDTO : taskDTOS) remove(taskDTO);
    }

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId};")
    void removeAllByUserId(@NotNull @Param("userId") String userId);

    @Update("UPDATE tm_task SET created = #{created}, name = #{name}, description = #{description}, " +
            "status = #{status}, project_id = #{projectId}, user_id = #{userId} WHERE id = #{id};")
    void update(@NotNull TaskDTO model);

    @Update("UPDATE tm_task SET project_id = null WHERE id = #{taskId};")
    void unbindTaskFromProject(@Nullable @Param("taskId") String taskId);

    default Collection<TaskDTO> set(@NotNull final Collection<TaskDTO> models) {
        removeAll();
        return addAll(models);
    }

}
