package ru.t1.kharitonova.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.endpoint.*;
import ru.t1.kharitonova.tm.api.service.*;
import ru.t1.kharitonova.tm.endpoint.*;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.exception.user.UserNotFoundException;
import ru.t1.kharitonova.tm.dto.model.ProjectDTO;
import ru.t1.kharitonova.tm.dto.model.TaskDTO;
import ru.t1.kharitonova.tm.dto.model.UserDTO;
import ru.t1.kharitonova.tm.service.*;
import ru.t1.kharitonova.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public final class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService, projectService, taskService);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService, projectService, taskService);

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(connectionService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService, sessionService);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final ICalculatorEndpoint calculatorEndpoint = new CalculatorEndpoint(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    @Getter
    private final Backup backup = new Backup(this);

    {
        registry(systemEndpoint);
        registry(userEndpoint);
        registry(domainEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(calculatorEndpoint);
        registry(authEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = "0.0.0.0";
        @NotNull final String port = "8080";
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }


    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    @SneakyThrows
    private void initDemoData() {
        if (userService.findByLogin("test") == null) {
            userService.create("test", "test", "test@test.ru");
        }
        if (userService.findByLogin("admin") == null) {
            userService.create("admin", "admin", Role.ADMIN);
        }

        @Nullable final UserDTO user_DTO_test = userService.findByLogin("test");
        @Nullable final UserDTO user_DTO_admin = userService.findByLogin("admin");
        if (user_DTO_test == null) throw new UserNotFoundException();
        if (user_DTO_admin == null) throw new UserNotFoundException();
        @NotNull final String user_test_id = user_DTO_test.getId();
        @NotNull final String user_admin_id = user_DTO_admin.getId();
        @Nullable final List<ProjectDTO> projectDTOS = projectService.findAll();
        @Nullable final List<TaskDTO> taskDTOS = taskService.findAll();

        if (projectDTOS == null || projectDTOS.stream().noneMatch(m -> "TEST PROJECT 1".equals(m.getName()))) {
            projectService.add(new ProjectDTO(user_test_id, "TEST PROJECT 1", "UserTest Project 1", Status.IN_PROGRESS));
        }
        if (projectDTOS.stream().noneMatch(m -> "TEST PROJECT 2".equals(m.getName()))) {
            projectService.add(new ProjectDTO(user_test_id, "TEST PROJECT 2", "UserTest Project 2", Status.NOT_STARTED));
        }
        if (projectDTOS.stream().noneMatch(m -> "TEST PROJECT 3".equals(m.getName()))) {
            projectService.add(new ProjectDTO(user_test_id, "TEST PROJECT 3", "UserAdmin Project 3", Status.IN_PROGRESS));
        }
        if (projectDTOS.stream().noneMatch(m -> "TEST PROJECT 4".equals(m.getName()))) {
            projectService.add(new ProjectDTO(user_test_id, "TEST PROJECT 4", "UserAdmin Project 4", Status.NOT_STARTED));
        }

        try {
            final String project_test_id = projectDTOS
                    .stream()
                    .filter(m -> "TEST PROJECT 1".equals(m.getName()))
                    .findFirst()
                    .orElse(null)
                    .getId();
            final String project_admin_id = projectDTOS
                    .stream()
                    .filter(m -> "TEST PROJECT 3".equals(m.getName()))
                    .findFirst()
                    .orElse(null)
                    .getId();

            if (taskDTOS == null || taskDTOS.stream().noneMatch(m -> "TEST TASK 1".equals(m.getName()))) {
                taskService.add(new TaskDTO(user_test_id, "TEST TASK 1", "DESCRIPTION 1", Status.IN_PROGRESS, null));
            }
            if (taskDTOS.stream().noneMatch(m -> "TEST TASK 2".equals(m.getName()))) {
                taskService.add(new TaskDTO(user_test_id, "TEST TASK 2", "DESCRIPTION 2", Status.NOT_STARTED, null));
            }
            if (taskDTOS.stream().noneMatch(m -> "TEST TASK 3".equals(m.getName()))) {
                taskService.add(new TaskDTO(user_test_id, "TEST TASK 3", "DESCRIPTION 3", Status.COMPLETED, project_test_id));
            }
            if (taskDTOS.stream().noneMatch(m -> "TEST TASK 4".equals(m.getName()))) {
                taskService.add(new TaskDTO(user_test_id, "TEST TASK 4", "DESCRIPTION 4", Status.NOT_STARTED, project_test_id));
            }
            if (taskDTOS.stream().noneMatch(m -> "TEST TASK 5".equals(m.getName()))) {
                taskService.add(new TaskDTO(user_admin_id, "TEST TASK 5", "DESCRIPTION 5", Status.COMPLETED, project_admin_id));
            }
            if (taskDTOS.stream().noneMatch(m -> "TEST TASK 6".equals(m.getName()))) {
                taskService.add(new TaskDTO(user_admin_id, "TEST TASK 6", "DESCRIPTION 6", Status.NOT_STARTED, null));
            }

        } catch (@NotNull final Exception e) {
            throw new Exception("Init Demo Data Exception");
        }
    }

    public void start() {
        initPID();
        initDemoData();
        loggerService.info("** WELCOME TO TASK MANAGER**");
        backup.init();
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
    }

    public void stop() {
        backup.stop();
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
    }

}
