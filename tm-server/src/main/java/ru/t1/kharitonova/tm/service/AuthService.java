package ru.t1.kharitonova.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.service.IAuthService;
import ru.t1.kharitonova.tm.api.service.IPropertyService;
import ru.t1.kharitonova.tm.api.service.ISessionService;
import ru.t1.kharitonova.tm.api.service.IUserService;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.exception.user.AccessDeniedException;
import ru.t1.kharitonova.tm.exception.user.LoginEmptyException;
import ru.t1.kharitonova.tm.exception.user.PasswordEmptyException;
import ru.t1.kharitonova.tm.exception.user.SessionTimeoutException;
import ru.t1.kharitonova.tm.dto.model.SessionDTO;
import ru.t1.kharitonova.tm.dto.model.UserDTO;
import ru.t1.kharitonova.tm.util.CryptUtil;
import ru.t1.kharitonova.tm.util.HashUtil;

import java.util.Date;

public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final ISessionService sessionService;

    public AuthService(@NotNull final IPropertyService propertyService,
                       @NotNull final IUserService userService,
                       @NotNull ISessionService sessionService) {
        this.propertyService = propertyService;
        this.userService = userService;
        this.sessionService = sessionService;
    }

    @Nullable
    @Override
    public UserDTO registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        return userService.create(login, password, email);
    }

    @Override
    public @NotNull UserDTO check(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDTO userDTO = userService.findByLogin(login);
        if (userDTO == null || userDTO.isLocked()) throw new AccessDeniedException();
        @Nullable String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(userDTO.getPasswordHash())) throw new AccessDeniedException();
        return userDTO;
    }

    @NotNull
    protected SessionDTO createSession(@NotNull final UserDTO userDTO) {
        @NotNull final SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setUserId(userDTO.getId());
        @NotNull final Role role = userDTO.getRole();
        sessionDTO.setRole(role);
        return sessionService.create(sessionDTO);
    }

    @Override
    public @NotNull String login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDTO userDTO = userService.findByLogin(login);
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (userDTO == null || userDTO.isLocked() || hash == null) throw new AccessDeniedException();
        if (!hash.equals(userDTO.getPasswordHash())) throw new AccessDeniedException();
        return getToken(userDTO);
    }

    @NotNull
    private String getToken(@NotNull final UserDTO userDTO) {
        return getToken(createSession(userDTO));
    }

    @SneakyThrows
    @NotNull
    private String getToken(@NotNull final SessionDTO sessionDTO) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String token = objectMapper.writeValueAsString(sessionDTO);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        return CryptUtil.encrypt(sessionKey, token);
    }

    @SneakyThrows
    @Override
    public SessionDTO validateToken(@Nullable final String token) {
        if (token == null) throw new AccessDeniedException();
        @NotNull String sessionKey = propertyService.getSessionKey();
        @NotNull String json;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
        } catch (@NotNull final Exception e) {
            throw new AccessDeniedException();
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final SessionDTO sessionDTO = objectMapper.readValue(json, SessionDTO.class);

        @NotNull Date currentDate = new Date();
        @NotNull Date sessionDate = sessionDTO.getCreated();
        final int timeout = propertyService.getSessionTimeout();
        if ((currentDate.getTime() - sessionDate.getTime()) / 1000 > timeout) {
            throw new SessionTimeoutException();
        }
        if (!sessionService.existsById(sessionDTO.getId())) throw new AccessDeniedException();
        return sessionDTO;
    }

    @Override
    public void invalidate(@Nullable final SessionDTO sessionDTO) {
        if (sessionDTO == null) return;
        sessionService.removeById(sessionDTO.getId());
    }

}
