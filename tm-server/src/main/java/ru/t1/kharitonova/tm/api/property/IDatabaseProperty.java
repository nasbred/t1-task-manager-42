package ru.t1.kharitonova.tm.api.property;

import org.jetbrains.annotations.NotNull;

public interface IDatabaseProperty {

    @NotNull
    String getDatabasePassword();

    @NotNull
    String getDatabaseUrl();

    @NotNull
    String getDatabaseUsername();

    @NotNull
    String getDatabaseDriver();

    @NotNull
    String getDBDialect();

    @NotNull
    String getDBHbm2ddlAuto();

    @NotNull
    String getDBShowSql();

    @NotNull
    String getDBFormatSql();

    @NotNull
    String getDBCommentsSql();

}
