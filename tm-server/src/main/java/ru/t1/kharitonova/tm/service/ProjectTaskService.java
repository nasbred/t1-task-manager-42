package ru.t1.kharitonova.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.repository.IProjectRepository;
import ru.t1.kharitonova.tm.api.repository.ITaskRepository;
import ru.t1.kharitonova.tm.api.service.IConnectionService;
import ru.t1.kharitonova.tm.api.service.IProjectService;
import ru.t1.kharitonova.tm.api.service.IProjectTaskService;
import ru.t1.kharitonova.tm.api.service.ITaskService;
import ru.t1.kharitonova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.kharitonova.tm.exception.entity.TaskNotFoundException;
import ru.t1.kharitonova.tm.exception.field.IndexIncorrectException;
import ru.t1.kharitonova.tm.exception.field.ProjectIdEmptyException;
import ru.t1.kharitonova.tm.exception.field.TaskIdEmptyException;
import ru.t1.kharitonova.tm.exception.field.UserIdEmptyException;
import ru.t1.kharitonova.tm.dto.model.ProjectDTO;
import ru.t1.kharitonova.tm.dto.model.TaskDTO;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IConnectionService connectionService;

    @NotNull
    public IProjectService projectService;

    @NotNull
    public ITaskService taskService;

    public ProjectTaskService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IProjectService projectService,
            @NotNull final ITaskService taskService
    ) {
        this.connectionService = connectionService;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Override
    @SneakyThrows
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
            if (!taskRepository.existsById(taskId)) throw new TaskNotFoundException();
            @Nullable TaskDTO taskDTO = taskRepository.findOneById(taskId);
            if (taskDTO == null) throw new TaskNotFoundException();
            taskDTO.setProjectId(projectId);
            taskRepository.update(taskDTO);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
            @Nullable final List<TaskDTO> taskDTOS = taskRepository.findAllByProjectId(userId, projectId);
            for (@NotNull final TaskDTO taskDTO : taskDTOS) {
                taskRepository.removeOneById(taskDTO.getId());
            }
            projectRepository.removeOneById(projectId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectByIndex(@Nullable final String userId, @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        if (index == null || index < 0 || index >= projectRepository.getSize()) throw new IndexIncorrectException();
        try {
            @Nullable ProjectDTO projectDTO = projectRepository.findOneByIndexByUserId(userId, index);
            if (projectDTO == null) throw new ProjectNotFoundException();
            @NotNull final String projectId = projectDTO.getId();
            @Nullable final List<TaskDTO> taskDTOS = taskRepository.findAllByProjectId(userId, projectId);
            for (@NotNull final TaskDTO taskDTO : taskDTOS) projectRepository.removeOneById(taskDTO.getId());
            projectRepository.removeOneByIndexForUser(userId, index);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
            @Nullable final TaskDTO taskDTO = taskRepository.findOneById(taskId);
            if (taskDTO == null) throw new TaskNotFoundException();
            taskDTO.setProjectId(null);
            taskRepository.update(taskDTO);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
