package ru.t1.kharitonova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.endpoint.IProjectEndpoint;
import ru.t1.kharitonova.tm.api.service.IProjectService;
import ru.t1.kharitonova.tm.api.service.IProjectTaskService;
import ru.t1.kharitonova.tm.api.service.IServiceLocator;
import ru.t1.kharitonova.tm.dto.request.project.*;
import ru.t1.kharitonova.tm.dto.response.project.*;
import ru.t1.kharitonova.tm.enumerated.ProjectSort;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.dto.model.ProjectDTO;
import ru.t1.kharitonova.tm.dto.model.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Comparator;
import java.util.List;

@WebService(endpointInterface = "ru.t1.kharitonova.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    @NotNull
    private IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectChangeStatusByIdResponse changeProjectStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectChangeStatusByIdRequest request
    ) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = sessionDTO.getUserId();
        @Nullable final Status status = request.getStatus();
        @Nullable final ProjectDTO projectDTO = getProjectService().changeProjectStatusById(userId, id, status);
        return new ProjectChangeStatusByIdResponse(projectDTO);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectChangeStatusByIndexRequest request
    ) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = sessionDTO.getUserId();
        @Nullable final Status status = request.getStatus();
        @Nullable final ProjectDTO projectDTO = getProjectService().changeProjectStatusByIndex(userId, index, status);
        return new ProjectChangeStatusByIndexResponse(projectDTO);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectCreateResponse createProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCreateRequest request
    ) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final String userId = sessionDTO.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final ProjectDTO projectDTO = getProjectService().create(userId, name, description);
        return new ProjectCreateResponse(projectDTO);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectListResponse listProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectListRequest request
    ) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final String userId = sessionDTO.getUserId();
        @Nullable final ProjectSort sort = request.getSort();
        @Nullable final Comparator<ProjectDTO> comparator = sort == null ? null : sort.getComparator();
        @NotNull final List<ProjectDTO> projectDTOS = getProjectService().findAll(userId, comparator);
        return new ProjectListResponse(projectDTOS);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectShowByIdResponse showProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectShowByIdRequest request
    ) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final String userId = sessionDTO.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final ProjectDTO projectDTO = getProjectService().findOneByIdAndUserId(userId, id);
        return new ProjectShowByIdResponse(projectDTO);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectShowByIndexResponse showProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectShowByIndexRequest request) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final String userId = sessionDTO.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final ProjectDTO projectDTO = getProjectService().findOneByIndex(userId, index);
        return new ProjectShowByIndexResponse(projectDTO);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectUpdateByIdResponse updateProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectUpdateByIdRequest request
    ) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final String userId = sessionDTO.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final ProjectDTO projectDTO = getProjectService().updateById(userId, id, name, description);
        return new ProjectUpdateByIdResponse(projectDTO);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectUpdateByIndexResponse updateProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectUpdateByIndexRequest request
    ) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final String userId = sessionDTO.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final ProjectDTO projectDTO = getProjectService().updateByIndex(userId, index, name, description);
        return new ProjectUpdateByIndexResponse(projectDTO);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectClearResponse clearProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectClearRequest request
    ) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final String userId = sessionDTO.getUserId();
        getProjectService().removeAllByUserId(userId);
        return new ProjectClearResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectRemoveByIdResponse removeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectRemoveByIdRequest request
    ) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final String userId = sessionDTO.getUserId();
        @Nullable final String id = request.getId();
        getProjectTaskService().removeProjectById(userId, id);
        return new ProjectRemoveByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectRemoveByIndexResponse removeProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectRemoveByIndexRequest request
    ) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final String userId = sessionDTO.getUserId();
        @Nullable final Integer index = request.getIndex();
        getProjectTaskService().removeProjectByIndex(userId, index);
        return new ProjectRemoveByIndexResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectStartByIdResponse startProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectStartByIdRequest request
    ) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = sessionDTO.getUserId();
        @Nullable final Status status = Status.IN_PROGRESS;
        @Nullable final ProjectDTO projectDTO = getProjectService().changeProjectStatusById(userId, id, status);
        return new ProjectStartByIdResponse(projectDTO);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectStartByIndexResponse startProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectStartByIndexRequest request
    ) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = sessionDTO.getUserId();
        @Nullable final Status status = Status.IN_PROGRESS;
        @Nullable final ProjectDTO projectDTO = getProjectService().changeProjectStatusByIndex(userId, index, status);
        return new ProjectStartByIndexResponse(projectDTO);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectCompleteByIdResponse completeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCompleteByIdRequest request
    ) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = sessionDTO.getUserId();
        @Nullable final Status status = Status.COMPLETED;
        @Nullable final ProjectDTO projectDTO = getProjectService().changeProjectStatusById(userId, id, status);
        return new ProjectCompleteByIdResponse(projectDTO);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectCompleteByIndexResponse completeProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCompleteByIndexRequest request
    ) {
        @NotNull final SessionDTO sessionDTO = check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = sessionDTO.getUserId();
        @Nullable final Status status = Status.COMPLETED;
        @Nullable final ProjectDTO projectDTO = getProjectService().changeProjectStatusByIndex(userId, index, status);
        return new ProjectCompleteByIndexResponse(projectDTO);
    }

}