package ru.t1.kharitonova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.model.SessionDTO;

public interface ISessionRepository {

    @Insert("INSERT INTO tm_session (id, created, role, user_id) " +
            "VALUES(#{id}, #{created}, #{role}, #{userId});")
    void add(@NotNull SessionDTO sessionDTO);

    @Delete("DELETE FROM tm_session WHERE id = #{id};")
    void removeById(@NotNull @Param("id") String id);

    @Delete("TRUNCATE TABLE tm_session;")
    void removeAll();

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId};")
    void removeAllByUserId(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_session WHERE id = #{id} LIMIT 1;")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @Nullable
    SessionDTO findOneById(@NotNull @Param("id") String id);

    @Select("SELECT * FROM tm_session WHERE user_id = #{userId} LIMIT 1;")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @Nullable
    SessionDTO findOneByUserId(@NotNull @Param("userId") String userId);

    @Select("SELECT COUNT(1) FROM tm_session;")
    int getSize();

}
