package ru.t1.kharitonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.dto.model.UserDTO;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IUserService {

    @Nullable
    UserDTO add(@NotNull UserDTO model);

    @NotNull
    Collection<UserDTO> addAll(@NotNull Collection<UserDTO> models);

    @NotNull
    Collection<UserDTO> set(@NotNull Collection<UserDTO> models);

    @Nullable
    List<UserDTO> findAll();

    @Nullable
    List<UserDTO> findAll(@NotNull Comparator<UserDTO> comparator);

    @Nullable
    UserDTO findOneById(@NotNull String id);

    @Nullable
    UserDTO findOneByIndex(@NotNull Integer index);

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String login);

    int getSize();

    boolean existsById(@NotNull String id);

    @Nullable
    UserDTO remove(@Nullable UserDTO model);

    void removeOneById(@NotNull String id);

    @Nullable
    UserDTO removeOneByIndex(@NotNull Integer index);

    void removeAll(@NotNull List<UserDTO> models);

    void removeAll();

    void update(@NotNull UserDTO model);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    UserDTO removeByLogin(@Nullable String login);

    @Nullable
    UserDTO removeByEmail(@Nullable String email);

    void setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    UserDTO updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    UserDTO lockUserByLogin(@Nullable String login);

    UserDTO unlockUserByLogin(@Nullable String login);

    @NotNull
    default Boolean isLoginExist(@NotNull String login) {
        return findByLogin(login) != null;
    }

    @NotNull
    default Boolean isEmailExist(@NotNull String email) {
        return findByEmail(email) != null;
    }

}
