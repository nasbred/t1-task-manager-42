package ru.t1.kharitonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.model.SessionDTO;

public interface ISessionService {

    @NotNull
    SessionDTO create(@Nullable SessionDTO sessionDTO);

    Boolean existsById(@Nullable String id);

    @Nullable
    SessionDTO findOneById(@Nullable String id);

    @Nullable
    SessionDTO findOneByUserId(@Nullable String id);

    void removeById(@Nullable String id);

    void removeAll();

    int getSize();

}
