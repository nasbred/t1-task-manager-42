package ru.t1.kharitonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.dto.model.ProjectDTO;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    void add(@NotNull ProjectDTO model);

    void add(@Nullable String userId, @NotNull ProjectDTO model);

    @NotNull
    Collection<ProjectDTO> addAll(@NotNull Collection<ProjectDTO> models);

    @NotNull
    ProjectDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    ProjectDTO changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    ProjectDTO changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Collection<ProjectDTO> set(@NotNull Collection<ProjectDTO> models);

    @Nullable
    List<ProjectDTO> findAll();

    @NotNull
    List<ProjectDTO> findAll(@NotNull String userId);

    @Nullable
    List<ProjectDTO> findAll(@NotNull Comparator<ProjectDTO> comparator);

    @NotNull
    List<ProjectDTO> findAll(@NotNull String userId, @Nullable Comparator<ProjectDTO> comparator);

    @Nullable
    ProjectDTO findOneById(@NotNull String id);

    @Nullable
    ProjectDTO findOneByIdAndUserId(@NotNull String user_id, @NotNull String id);

    @Nullable
    ProjectDTO findOneByIndex(@NotNull Integer index);

    @Nullable
    ProjectDTO findOneByIndex(@Nullable String userId, @Nullable Integer index);

    int getSize();

    int getSize(@NotNull String userId);

    boolean existsById(@NotNull String id);

    boolean existsById(@NotNull String userId, @NotNull String id);

    void remove(@Nullable ProjectDTO model);

    void removeOneById(@NotNull String id);

    void removeOneById(@Nullable String userId, @Nullable String id);

    void removeOneByIndex(@NotNull Integer index);

    void removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    void removeAll();

    void removeAll(@NotNull List<ProjectDTO> models);

    void removeAllByUserId(@NotNull String userId);

    void update(@NotNull ProjectDTO model);

    @NotNull
    ProjectDTO updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    ProjectDTO updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

}
