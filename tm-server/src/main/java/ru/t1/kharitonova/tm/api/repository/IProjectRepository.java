package ru.t1.kharitonova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.kharitonova.tm.dto.model.ProjectDTO;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO tm_project (id, name, description, status, created, user_id) " +
            "VALUES(#{id}, #{name}, #{description}, #{status}, #{created}, #{userId});")
    void add(@NotNull ProjectDTO projectDTO);

    default Collection<ProjectDTO> addAll(@NotNull final Collection<ProjectDTO> models) {
        @NotNull List<ProjectDTO> result = new ArrayList<>();
        for (@NotNull final ProjectDTO model : models) {
            add(model);
            result.add(model);
        }
        return result;
    }

    @Update("UPDATE tm_project SET status = #{status} WHERE id = #{id} AND user_id = #{userId};")
    void changeStatusById(
            @Nullable @Param("userId") String userId,
            @Nullable @Param("id") String id,
            @Nullable @Param("status") Status status
    );

    default boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Select("SELECT * FROM tm_project;")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @NotNull
    List<ProjectDTO> findAll();

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId};")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @NotNull
    List<ProjectDTO> findAllByUserId(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY #{sort};")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @NotNull
    List<ProjectDTO> findAllByUserIdWithSort(@NotNull @Param("userId") String userId, @NotNull @Param("sort") String sort);

    @Select("SELECT * FROM tm_project WHERE id = #{id} LIMIT 1;")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @Nullable
    ProjectDTO findOneById(@Param("id") @NotNull String id);

    @Select("SELECT * FROM tm_project WHERE id = #{id} AND user_id = #{user_id} LIMIT 1;")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @Nullable
    ProjectDTO findOneByIdAndUserId(@Param("user_id") @NotNull String user_id, @Param("id") @NotNull String id);

    @Select("SELECT * FROM tm_project OFFSET #{index} LIMIT 1;")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @Nullable
    ProjectDTO findOneByIndex(@Param("index") @NotNull Integer index);

    @Select("SELECT * FROM tm_project WHERE user_id = #{user_id} OFFSET #{index} LIMIT 1;")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @Nullable
    ProjectDTO findOneByIndexByUserId(
            @NotNull @Param("user_id") String user_id,
            @NotNull @Param("index") Integer index
    );

    @Select("SELECT COUNT(1) FROM tm_project;")
    int getSize();

    @Select("SELECT COUNT(1) FROM tm_project WHERE user_id = #{user_id};")
    int getSizeForUser(@NotNull @Param("user_id") String user_id);

    default void remove(@Nullable final ProjectDTO model) {
        if (model == null) throw new ProjectNotFoundException();
        removeOneById(model.getId());
    }

    @Delete("DELETE FROM tm_project WHERE id = #{id};")
    void removeOneById(@NotNull @Param("id") String id);

    default ProjectDTO removeOneByIndex(@NotNull final Integer index) {
        @Nullable final ProjectDTO model = findOneByIndex(index);
        if (model == null) return null;
        remove(model);
        return model;
    }

    default ProjectDTO removeOneByIndexForUser(@NotNull String userId, @NotNull final Integer index) {
        @Nullable final ProjectDTO model = findOneByIndexByUserId(userId, index);
        if (model == null) return null;
        remove(model);
        return model;
    }

    @Delete("TRUNCATE TABLE tm_project;")
    void removeAll();

    default void removeAllByList(@NotNull List<ProjectDTO> projectDTOS) {
        for (@NotNull final ProjectDTO projectDTO : projectDTOS) remove(projectDTO);
    }

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId};")
    void removeAllByUserId(@NotNull @Param("userId") String userId);

    default Collection<ProjectDTO> set(@NotNull final Collection<ProjectDTO> models) {
        removeAll();
        return addAll(models);
    }

    @Update("UPDATE tm_project SET created = #{created}, name = #{name}, description = #{description}, " +
            "status = #{status}, user_id = #{userId} WHERE id = #{id};")
    void update(@NotNull ProjectDTO model);

}
