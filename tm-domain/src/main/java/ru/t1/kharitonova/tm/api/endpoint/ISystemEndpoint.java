package ru.t1.kharitonova.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.dto.request.system.ServerAboutRequest;
import ru.t1.kharitonova.tm.dto.request.system.ServerVersionRequest;
import ru.t1.kharitonova.tm.dto.response.system.ServerAboutResponse;
import ru.t1.kharitonova.tm.dto.response.system.ServerVersionResponse;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface ISystemEndpoint extends IEndPoint {

    @NotNull
    String NAME = "SystemEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndPoint.newInstance(connectionProvider, NAME, SPACE, PART, ISystemEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndPoint.newInstance(host, port, NAME, SPACE, PART, ISystemEndpoint.class);
    }

    @NotNull
    ServerAboutResponse getAbout(@NotNull ServerAboutRequest request);

    @NotNull
    ServerVersionResponse getVersion(@NotNull ServerVersionRequest request);

    @WebMethod(exclude = true)
    static void main(String[] args) {
        System.out.println(ISystemEndpoint.newInstance().getAbout(new ServerAboutRequest()));
        System.out.println(ISystemEndpoint.newInstance().getVersion(new ServerVersionRequest()));
    }

}
