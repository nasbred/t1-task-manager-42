package ru.t1.kharitonova.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.comparator.CreatedComparator;
import ru.t1.kharitonova.tm.comparator.NameComparator;
import ru.t1.kharitonova.tm.comparator.StatusComparator;
import ru.t1.kharitonova.tm.dto.model.ProjectDTO;

import java.util.Comparator;

public enum ProjectSort {

    BY_NAME("Sort by name", NameComparator.INSTANCE::compare),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE::compare),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE::compare);

    @Getter
    @NotNull
    private final String displayName;

    @Getter
    @NotNull
    private final Comparator<ProjectDTO> comparator;

    ProjectSort(@NotNull final String displayName, @NotNull final Comparator<ProjectDTO> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public static ProjectSort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@NotNull final ProjectSort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

}
